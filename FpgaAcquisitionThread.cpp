#include "Fpga.h"
#include "FpgaClass.h"
#include "FpgaAcquisitionThread.h"
#include "time.h"
//#include <device.h>
//#include <tango.h>
#include <string>
#include <stdlib.h>
#include <cstring>
#include <numeric>

namespace Fpga_ns
{

FpgaAcquisitionThread::FpgaAcquisitionThread(Fpga *_fpga, int _adq_num, omni_mutex &d_m, omni_mutex &g_m) : Tango::LogAdapter(_fpga), omni_thread(), data_mutex(d_m), group_mutex(g_m)
{
    fpga = _fpga;
    adq_num = _adq_num;

    adq_cu = fpga->adq_cu;
    // last_update.tv_sec = 0;
    // last_update.tv_usec = 0;
    auto last_update = Time::now();

    // define get_name to yield name of class from first argument.

    DEBUG_STREAM << "AcquisitionThread::AcquisitionThread(): go to start thread ...\n";
    start_undetached();
}

//+------------------------------------------------------------------
/**
 * method: get_name()
 * Get device name.
 *
 * Return the device name (dev_name field)
 *
 * @return Device name
 */

string FpgaAcquisitionThread::get_name()
{
    return fpga->device_name;
}

//+------------------------------------------------------------------
/**
 * method: set_state()
 * set Fpga class` state.
 *
 * @return void
 */

void FpgaAcquisitionThread::set_state(const Tango::DevState &new_state)
{
    fpga->set_state(new_state);
}

//+------------------------------------------------------------------
/**
 * method: delete_device()
 * delete Fpga class` instance.
 */

void FpgaAcquisitionThread::delete_device()
{
    fpga->delete_device();
}

//+------------------------------------------------------------------
/**
 *  method: run_undetached()
 *
 *  description:
 *       _       _                             _             _
      __| | __ _| |_ __ _   _ __ ___  __ _  __| | ___  _   _| |_
     / _` |/ _` | __/ _` | | '__/ _ \/ _` |/ _` |/ _ \| | | | __|
    | (_| | (_| | || (_| | | | |  __/ (_| | (_| | (_) | |_| | |_
     \__,_|\__,_|\__\__,_| |_|  \___|\__,_|\__,_|\___/ \__,_|\__|

 *
 *
 */
//+------------------------------------------------------------------
void *FpgaAcquisitionThread::run_undetached(void *)
{
    bool exit;
    bool stop;

    DEBUG_STREAM << "FpgaAcquisitionThread::run_undetached: adq_num: " << adq_num << endl;
    // unsigned int time_start, time_start_waveform, time_end, time_end_waveform, timeout_start, timeout_end;

    // this needs doing later
    auto time_start = Time::now();
    auto time_start_waveform = Time::now();
    auto time_end = Time::now();
    auto time_end_waveform = Time::now();
    auto timeout_start = Time::now();
    auto timeout_end = Time::now();

    unsigned long acqTimeout = fpga->acqTimeout;

    updatePeriod = fpga->acqUpdatePeriod;

    int ch;
    unsigned int acquisition_stop = 0;
    unsigned int waveforms_accumulated;
    unsigned int waveforms_accumulated_old;
    unsigned int data_available, in_idle, timeout_init;
    unsigned int transfer_delay;
    unsigned int timestep_resolution;
    unsigned int timestep_multiplier;
    unsigned int buffers_filled_max = 0;
    unsigned int buffers_filled_old = 0xCDCDCDCD;
    unsigned int buffers_filled_max_old = 0xCDCDCDCD; //Just used to update the buffers status. Nothing else.
    unsigned int buffer_retry_counter = 0;
    unsigned int ShutdownStatus = 0;

    double time_elapsed_msec = 0;
    double timeout_elapsed_msec = 0;
    unsigned int buffers_filled = 0;
    unsigned int collect_result = 0;

    unsigned long long int collected_waveforms = 0;

    unsigned int buffer_error = 0;
    unsigned int overflow = 0;

    long last_error;
    unsigned long long int sent_waveforms = 0;
    unsigned int samples_rest = 0;
    unsigned int sample_counter = 0;
    unsigned int samples_in_channel_buffer = 0;

    unsigned int collect_retry_counter = 0;

    unsigned int waveforms_in_channel_buffer;
    unsigned long long int waveform_counter = 0;
    unsigned long long int waveform_counter_old = 0;

    int i = 0;
    std::string x[] = {
        "/",
        "-",
        "\\",
        "|",
    };

    int nofchannels = fpga->nofchannels;
    // point the tango type pointer to our data.

    int channelsPerCard = fpga->channelsPerCard;
    int samples_per_waveform;
    unsigned long long int wf = 0; //Used for looping, not for other things
    unsigned long long int waveforms_per_channel;
    unsigned long long int new_waveforms = 0;
    unsigned int analog_waveforms_to_collect;
    double delay;

    // make this address local to this thread
    signed int *data_stream;
    int acq_fini;
    
    DEBUG_STREAM << adq_num << ": FpgaAcquisitionThread::run_undetached() entering endless loop!" << endl;

    while (1)
    {
        {
            // copy to local variables
            omni_mutex_lock l(data_mutex);

            exit = fpga->exit_thread;
        }

        /* exit the calculation thread */
        if (exit == true)
        {
            DEBUG_STREAM << "\n\n"
                         << adq_num << ": Exit variable is " << exit << "!\n\n"
                         << endl;
            break;
        }

        // I don't want lots of lines here. so look at logger and then print
        // chars myself.
        if (get_logger()->is_info_enabled())
        {
            i++;
            cout << "\r" << adq_num << x[i % 4] << std::flush;
        }
        // struct timeval now;
        // gettimeofday(&now, NULL);
        auto tnow = Time::now();

        // double delay = (now.tv_sec + now.tv_usec) - (last_update.tv_sec * 1e6 + last_update.tv_usec);
        delay = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - last_update).count();

        // only read new data every updatePeriod ms!!
        if (delay < updatePeriod)
        {
            usleep(100); // used to be 50 ms, but we missed some triggers
            continue;
        }
        else
        {
            // DEBUG_STREAM << "delay = " << delay/1000 << "ms" << endl;

            last_update = Time::now();
        }

        {
            // make sure the state is freely accessible
            omni_mutex_lock l(data_mutex);
            if (fpga->get_state() != Tango::EXTRACT)
            {
                continue;
            }
        }
        if (get_logger()->is_info_enabled())
        {
            cout << endl;
        }
        DEBUG_STREAM << "Start loop for adq_num " << adq_num << "!" << endl;
        // reset the variables for the next acquisition
        sent_waveforms = 0;
        collect_result = 0;
        buffers_filled = 0;
        collect_retry_counter = 0;

        collect_retry_counter = 0;
        samples_per_waveform = fpga->samples_per_waveform;

        waveform_counter = 0;
        waveform_counter_old = 0;
        samples_in_channel_buffer = 0;
        acquisition_stop = 0;
        waveforms_accumulated;
        waveforms_accumulated_old;
        data_available, in_idle, timeout_init = 0;
        buffers_filled_max = 0;
        buffers_filled_old = 0xCDCDCDCD;
        buffers_filled_max_old = 0xCDCDCDCD; //Just used to update the buffers status. Nothing else.
        buffer_retry_counter = 0;
        ShutdownStatus = 0;
        collected_waveforms = 0;
        buffer_error = 0;
        overflow = 0;

        sent_waveforms = 0;
        samples_rest = 0;
        sample_counter = 0;
        samples_in_channel_buffer = 0;

        collect_retry_counter = 0;

        waveforms_in_channel_buffer = 0;
        waveform_counter = 0;
        waveform_counter_old = 0;
        waveforms_per_channel = 0;
        new_waveforms = 0;
        analog_waveforms_to_collect = fpga->analog_waveforms_to_collect;

        // set the acq_finished flag to true, the logic is inverted, true is not finished, false is finished
        // this allows the summing of all the threads acq_fini and only if all false will the last thread
        // proceed to the autosave.
        fpga->acq_fini[adq_num] = 1;

        // gettimeofday(&tv, NULL);
        // time_start = tv.tv_usec;
        // time_start_waveform = tv.tv_usec;
        // timeout_start = tv.tv_usec;
        auto time_start = Time::now();
        timeout_elapsed_msec = 0;

        int retval, actual;
        // int **stream_data_target = 0;

        // At this point everything should have been setup correctly and we enter the main acquisition loop
        do
        {
        try {
            std::string msg, m;
            
            //Simple time consumption measurement. For better accuracy use other method.
            if (collected_waveforms > 0)
            {
                // gettimeofday(&tv, NULL);
                // time_start_waveform = auto time_start = Time::now();
                time_start_waveform = Time::now();
            }

            DEBUG_STREAM << adq_num << ": sent_waveforms: " << sent_waveforms << " of " << analog_waveforms_to_collect << endl;
            //If acquisition needs to be repeated indefinitely or if the host has not received all averaged waveforms
            if (sent_waveforms < analog_waveforms_to_collect)
            {
                acquisition_stop = 0;
                waveforms_accumulated = fpga->waveforms_accumulated;
                waveforms_accumulated_old = 0;
                // gettimeofday(&tv, NULL);
                // timeout_start = tv.tv_usec;
                timeout_start = Time::now();
                CHECKADQ(Numbat_WfaArm(adq_cu, adq_num));
                timestep_resolution = fpga->attr_TimestepResolution_read[0];
                timestep_multiplier = fpga->attr_TimestepMultiplier_read[0];

                //This loop mainly checks the FPGA for data available. Once there is data to transfer this loop will exit.
                do
                { //Acquisition loop

                    // Get the data available signal
                    // DEBUG_STREAM << adq_num << ": Numbat_WfaGetStatus 1 ... " << endl;
                    CHECKADQ(Numbat_WfaGetStatus(adq_cu, adq_num, &data_available, &waveforms_accumulated, &in_idle));
                    // DEBUG_STREAM << "passed."
                    //              << " data_available " << data_available << " waveforms_accumulated " << waveforms_accumulated << " in_idle " << in_idle;

                    //Only update the printout if number of accumulated waveforms has changed
                    // DEBUG_STREAM << adq_num << ": waveforms_accumulated: " << waveforms_accumulated << " old: " << waveforms_accumulated_old;
                    if (waveforms_accumulated > waveforms_accumulated_old)
                    {
                        if (timeout_init > 0)
                        {
                            // INFO_STREAM << "\n";
                            timeout_init = 0;
                        }
                        // gettimeofday(&tv, NULL);
                        // time_end = tv.tv_usec; // this value is in microseconds
                        // time_elapsed_msec = (double)(time_end - time_start) / 1000. ;
                        time_end = Time::now();
                        time_elapsed_msec = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - time_start).count();

                        //TE  = Time Elapsed
                        //Acc = Accumulated on FPGA
                        //Rec = Received
                        //Sent = Sent from FPGA
                        //Avai = Available on FPGA
                        if (get_logger()->is_info_enabled())
                        {
                            cout << "\r[1TE - " << adq_num << ": " << time_elapsed_msec << " ms] [Acc: " << waveforms_accumulated << "] [Sent="
                                 << sent_waveforms << "] [Rec=" << collected_waveforms << "] [Avai=" << data_available << "]"
                                 << "           " << std::flush;
                        }

                        waveforms_accumulated_old = waveforms_accumulated;
                        // gettimeofday(&tv, NULL);
                        // timeout_start = tv.tv_usec;
                        timeout_start = Time::now();
                    }
                    else
                    {
                        if (get_logger()->is_info_enabled())
                        {
                            cout << "\r[Timeout counter: " << timeout_elapsed_msec << " ms] [Timeout Limit: " << acqTimeout << " ms]" << std::flush;
                        }
                        // if (time_elapsed_msec > accumulation_timeout)
                        // {
                        //     // DEBUG_STREAM << "TIMEOUT !!!!! You should act !" << endl;
                        //     ;
                        //     //     fpga->set_state(Tango::FAULT);
                        //     //     acquisition_stop = 1;
                        //     //     waveforms_accumulated = 0;
                        //     //     stop = true;
                        // }
                        timeout_init = 1;
                        Sleep(20);
                    }
                    // INFO_STREAM << endl;

                    CHECKADQ(Numbat_GetStreamOverflow(adq_cu, adq_num, &overflow));
                    if (overflow)
                    {
                        string m;
                        m = to_string(adq_num) + ": WARNING! STREAMING OVERFLOW! Overflow status = " + to_string(overflow) + "\n";
                        msg += m;
                        ERROR_STREAM << "\n" << msg << endl;
                        acquisition_stop = 1;
                        waveforms_accumulated = 0;
                        fpga->acq_fini[adq_num] = 0;
                        ShutdownStatus = 1;
                        stop = true;
                        fpga->acquisition_state = Tango::FAULT;
                        Tango::Except::throw_exception ( (const char *)"Fpga Acquisition",
                                msg.c_str(),
                                (const char *)"AcquisitionThread::run_undetached"); 
                        break;  // is there a break after an exception ?
                    }

                    // a few lines less than with the former firmware
                    // gettimeofday(&tv, NULL);
                    // timeout_end = tv.tv_usec;
                    // timeout_elapsed_msec = (double)(timeout_end - timeout_start);
                    timeout_end = Time::now();
                    timeout_elapsed_msec = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - timeout_start).count();

                    // break the calculation thread
                    if (stop || timeout_elapsed_msec > acqTimeout)
                    {
                        // If timeout, then we need to set stop, keeps DS from saving
                        stop = true;
                        fpga->acquisition_state = Tango::FAULT;
                        if (timeout_elapsed_msec > acqTimeout) {
                            DEBUG_STREAM << "TIMEOUT !!!!!" << endl;
                        } else {
                            string m;
                            m = to_string(adq_num) + ": Acquisition has been stopped ! \n";
                            msg += m;
                            ERROR_STREAM << "\n" << msg << endl;
                        }
                        acquisition_stop = 1;
                        waveforms_accumulated = 0;
                        buffers_filled = buffers_filled_old = 0;
                        fpga->acq_fini[adq_num] = 0;
                        ShutdownStatus = 1;
                        break;
                    }

                    // Sleep(20);
                } while ((acquisition_stop < 1) && !data_available && (waveforms_accumulated < fpga->number_of_waveforms_to_average));

                if (get_logger()->is_info_enabled())
                {
                    cout << endl;
                }

                //At this point, there is exactly 1 analog and 12 digital waveforms sitting in the FPGA memory waiting to be transfered.
                //Send GetWaveForm command to device to get the data.
                if (waveforms_accumulated > 0)
                {
                    DEBUG_STREAM << adq_num << ": Transfering waveform..." << endl;

                    //Must repeatedly issue transfer data pulse and wait until state machine has reached idle
                    //because the command might be missed if it arrives at the wrong state.
                    do
                    {
                        CHECKADQ(Numbat_WfaGetWaveform(adq_cu, adq_num));
                        CHECKADQ(Numbat_WfaGetStatus(adq_cu, adq_num, NULL, NULL, &in_idle));
                        if (get_logger()->is_info_enabled())
                        {
                            cout << "\r idle ? " << in_idle << " " << x[i++ % 4] << std::flush;
                        }
                    } while (!in_idle);
                    if (get_logger()->is_info_enabled())
                    {
                        i++;
                        cout << endl;
                    }

                    //Once in_idle is asserted the analog part has been transfered out of the WFA module. But the digital part might still be in progress

                    //Calculate appropriate delay before flushing. This is nessessary especially when timestep is very large
                    //Flushing too early might result in flush data overwriting averaged data.
                    //This might not be needed if the flush command can check for transfer progress status.
                    //transfer_delay = (2*timestep_resolution*timestep_multiplier*samples_per_waveform+1000000)/1000000;
                    //Sleep(transfer_delay);

                    //Every exit of the Acquisition loop above means that one averaged analog waveform has been created and sent.
                    //Each analog waveform per channel transfered, will be followed by 12 digital waveforms transfer.
                    sent_waveforms++;
                }
                else
                {
                    DEBUG_STREAM << adq_num << ": Numbat_WfaGetStatus 2 ... " << endl;
                    CHECKADQ(Numbat_WfaGetStatus(adq_cu, adq_num, &data_available, &waveforms_accumulated, &in_idle));
                    DEBUG_STREAM << "passed."
                                 << " data_available " << data_available << " waveforms_accumulated " << waveforms_accumulated << " in_idle " << in_idle;
                    // gettimeofday(&tv, NULL);
                    // time_end = tv.tv_usec;
                    // time_elapsed_msec = (double)(time_end - time_start) * 1000.;
                    time_elapsed_msec = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - time_start).count();

                    if (get_logger()->is_info_enabled())
                    {
                        cout << "[2TE - " << adq_num << ": " << time_elapsed_msec << " ms] [Acc: " << waveforms_accumulated << "] [Sent="
                             << sent_waveforms << "] [Rec=" << collected_waveforms << "] [Avai=" << data_available << "]" << std::flush;
                    }
                    stop = true;
                    fpga->acquisition_state = Tango::FAULT;
                    m = "ERROR: Acquisition failed ! No waveforms accumulated!\n";
                    msg += m;
                    ERROR_STREAM << msg << endl;
                    Tango::Except::throw_exception ( (const char *)"Fpga Acquisition",
						     msg.c_str(),
						     (const char *)"AcquisitionThread::run_undetached"); 
                }

                //Final readout of number of accumulated waveforms.
                //This readout must be put here in order to readout the correct number of accumulated waveforms.
                DEBUG_STREAM << adq_num << ": Numbat_WfaGetStatus 3 ... " << endl;
                CHECKADQ(Numbat_WfaGetStatus(adq_cu, adq_num, &data_available, &waveforms_accumulated, &in_idle));
                DEBUG_STREAM << "passed."
                             << " data_available " << data_available << " waveforms_accumulated " << waveforms_accumulated << " in_idle " << in_idle;
                // gettimeofday(&tv, NULL);
                // time_end = tv.tv_usec;
                // time_elapsed_msec = (double)(time_end - time_start) * 1000.;
                time_elapsed_msec = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - time_start).count();
                if (get_logger()->is_info_enabled())
                {
                    cout << "\r[3TE - " << adq_num << " : " << time_elapsed_msec << " ms] [Acc: " << waveforms_accumulated << "] [Sent=" << sent_waveforms << "] [Rec=" << collected_waveforms << "] [Avai=" << data_available << "]";
                }

                //Flush out the last waveform by disarming wfa so that the last buffer can be filled.
                if (sent_waveforms >= analog_waveforms_to_collect)
                {
                    //It's more effective to turn off timestep before flushing out the data
                    CHECKADQ(Numbat_SetTimeStepSize(adq_cu, adq_num, 0, 0, 0));
                    CHECKADQ(Numbat_FlushData(adq_cu, adq_num));
                    Sleep(50);
                }
            }

            // Check streaming buffer status
            DEBUG_STREAM << adq_num << ": ADQ_GetTransferBufferStatus() ... " << endl;
            CHECKADQ(ADQ_GetTransferBufferStatus(adq_cu, adq_num, &buffers_filled));
            DEBUG_STREAM << "passed. buffers_filled " << buffers_filled << " with buffers_filled_max " << buffers_filled_max << endl;

            if (buffers_filled_max < buffers_filled)
            {
                buffers_filled_max = buffers_filled;
            }

            //This if case is just for updating the buffer status if the status has changed.
            //This is to prevent filling the screen with unnecessary printout.
            if ((buffers_filled != buffers_filled_old) || (buffers_filled_max != buffers_filled_max_old))
            {
                INFO_STREAM << "Buffers currently filled: " << buffers_filled << " (peak" << buffers_filled_max
                            << "). Buffer retry counter:" << buffer_retry_counter << endl;
                buffers_filled_old = buffers_filled;
                buffers_filled_max_old = buffers_filled_max;
                Sleep(100);
            }

            // If buffers_filled is not what you expect, it probably means that the transfer has timed out
            // Use ADQ_SetTransferTimeout to increase the timeout.
            if (buffers_filled > 0)
            {
                DEBUG_STREAM << adq_num << ": ADQ_CollectDataNextPage() ... " << endl;
                collect_result = ADQ_CollectDataNextPage(adq_cu, adq_num);
                DEBUG_STREAM << "passed. collect_result " << collect_result << endl;
                if (collect_result)
                {
                    // copy to local variables

                    //Every time the program pass through here, 1 transfer buffer will be parsed and separated into respective channel
                    //memcpy((void*)data_stream, ADQ_GetPtrStream(adq_cu, adq_num), 256*nofchannels*sizeof(int));
                    data_stream = (int *)ADQ_GetPtrStream(adq_cu, adq_num);
                    DEBUG_STREAM << adq_num << ": ADQ_GetPtrStream(adq_cu, adq_num): " << data_stream << endl;

                    //Separate data into channels
                    //The data is grouped in 1024 bytes from each channel in the buffer.
                    //[1024 bytes from A, 1024 bytes from B, 1024 bytes from C, 1024 bytes from D, 1024 bytes from A,...repeat]
                    //Note: 256 samples * 4 bytes = 1024 bytes
                    // DEBUG_STREAM << "Transferring " << 256 * sizeof(int) << " bytes - stream_data_target from data_stream" << endl;
                    for (sample_counter = 0; sample_counter < fpga->tbuffer_size_samples / nofchannels; sample_counter += 256)
                    {
                        // DEBUG_STREAM << &(fpga->stream_data_target[0])[samples_in_channel_buffer + sample_counter]  +  256 * sizeof(int) << endl;
                        memcpy((void *)&(fpga->stream_data_target[0])[samples_in_channel_buffer + sample_counter], &data_stream[1024 * (sample_counter / 256) + 256 * 0], 256 * sizeof(int));
                        // DEBUG_STREAM << &(fpga->stream_data_target[0])[samples_in_channel_buffer + sample_counter]  +  256 * sizeof(int) << endl;
                        memcpy((void *)&(fpga->stream_data_target[1])[samples_in_channel_buffer + sample_counter], &data_stream[1024 * (sample_counter / 256) + 256 * 1], 256 * sizeof(int));
                        // DEBUG_STREAM << &(fpga->stream_data_target[2])[samples_in_channel_buffer + sample_counter]  +  256 * sizeof(int) << endl;
                        memcpy((void *)&(fpga->stream_data_target[2])[samples_in_channel_buffer + sample_counter], &data_stream[1024 * (sample_counter / 256) + 256 * 2], 256 * sizeof(int));
                        // DEBUG_STREAM << &(fpga->stream_data_target[3])[samples_in_channel_buffer + sample_counter]  +  256 * sizeof(int) << endl;
                        memcpy((void *)&(fpga->stream_data_target[3])[samples_in_channel_buffer + sample_counter], &data_stream[1024 * (sample_counter / 256) + 256 * 3], 256 * sizeof(int));
                    }

                    samples_in_channel_buffer = samples_in_channel_buffer + sample_counter;
                    waveforms_in_channel_buffer = samples_in_channel_buffer / samples_per_waveform; //This number is including the digital waveform as well

                    if (fpga->pulse_counter_average_en) // until Jan 2019, pulse_counter_average_en was always 1
                    {
                        waveforms_per_channel = waveforms_in_channel_buffer / 4; //This number is PER each 16 channels. So if this number is 5, it means that EVERY 16 channels already transferred 5 waveforms
                        // DEBUG_STREAM << "\nsamples_per_waveform: " << samples_per_waveform << " waveforms_in_channel_buffer: " << waveforms_in_channel_buffer << "\n" << endl;

                        int row;
                        // Annotation HW: 16/11/2018: no clue if the 4 further down is the number of channels. :-(
                        //Further separation into 4 real analog channel data and 8 digital virtual channel data
                        for (ch = 0; ch < nofchannels; ch++)
                        {
                            // While using the SPS, we're dealing with one piece of memory for the 12 channels. No distiction between analog and digital.
                            // DEBUG_STREAM << "Transferring " << samples_per_waveform * sizeof(int) << " bytes - analog_data_target from stream_data_target" << endl;
                            // sorry, but I just don't understand the following loop :-(
                            for (wf = 0; wf < waveforms_in_channel_buffer - (waveforms_in_channel_buffer % 4); wf = wf + 4)
                            {
                                row = ch * nofchannels + 0;
                                // to SCAN type SPS 
                                // memcpy((void *)&(fpga->analog_data_target[ch + 0])[(wf / 4) * samples_per_waveform], (void *)&(fpga->stream_data_target[ch])[(wf * samples_per_waveform) + samples_per_waveform * 0], samples_per_waveform * sizeof(int));
                                if ((retval = SPS_CopyColToShared(fpga->SPS_channels, fpga->wfsps, &(fpga->stream_data_target[ch])[(wf * samples_per_waveform) + samples_per_waveform * 0], fpga->type, row, sizeof(int) * samples_per_waveform, &actual)))
                                {
                                    stop = true;
                                    fpga->acquisition_state = Tango::FAULT;
                                    msg = "SPS_CopyColToShared ch=" + to_string(ch) + " wf=" + to_string(wf) + " failed!";
                                    ERROR_STREAM << msg << endl;
                                    Tango::Except::throw_exception ( (const char *)"Fpga Acquisition",
				            		     msg.c_str(),
						                 (const char *)"AcquisitionThread::run_undetached"); 
                                }
                                // to image type SPS
                                char *spsimg = const_cast<char*>(fpga->chnames[row].data());
                                if ((retval = SPS_CopyRowToShared(fpga->SPS_scan, spsimg, &(fpga->stream_data_target[ch])[(wf * samples_per_waveform) + samples_per_waveform * 0], fpga->type, fpga->NPTS, sizeof(int) * samples_per_waveform, &actual)))
                                {
                                    stop = true;
                                    fpga->acquisition_state = Tango::FAULT;
                                    msg = "SPS_CopyRowToShared ch=" + to_string(ch) + " row=" + to_string(row) + " failed!";
                                    ERROR_STREAM << msg << endl;
                                    Tango::Except::throw_exception ( (const char *)"Fpga Acquisition",
				            		     msg.c_str(),
						                 (const char *)"AcquisitionThread::run_undetached"); 
                                }

                                row = ch * nofchannels + 1;
                                // memcpy((void *)&(fpga->digital_data_target[ch + 0])[(wf / 4) * samples_per_waveform], (void *)&(fpga->stream_data_target[ch])[(wf * samples_per_waveform) + samples_per_waveform * 1], samples_per_waveform * sizeof(int));
                                if ((retval = SPS_CopyColToShared(fpga->SPS_channels, fpga->wfsps, &(fpga->stream_data_target[ch])[(wf * samples_per_waveform) + samples_per_waveform * 1], fpga->type, row, sizeof(int) * samples_per_waveform, &actual)))
                                {
                                    stop = true;
                                    fpga->acquisition_state = Tango::FAULT;
                                    msg = "SPS_CopyColToShared ch=" + to_string(ch) + " wf=" + to_string(wf) + " failed!";
                                    ERROR_STREAM << msg << endl;
                                    Tango::Except::throw_exception ( (const char *)"Fpga Acquisition",
				            		     msg.c_str(),
						                 (const char *)"AcquisitionThread::run_undetached"); 
                                }
                                // to image type SPS
                                spsimg = const_cast<char*>(fpga->chnames[row].data());
                                if ((retval = SPS_CopyRowToShared(fpga->SPS_scan, spsimg, &(fpga->stream_data_target[ch])[(wf * samples_per_waveform) + samples_per_waveform * 1], fpga->type, fpga->NPTS, sizeof(int) * samples_per_waveform, &actual)))
                                {
                                    fpga->acquisition_state = Tango::FAULT;
                                    stop = true;
                                    msg = "SPS_CopyRowToShared ch=" + to_string(ch) + " row=" + to_string(row) + " failed!";
                                    ERROR_STREAM << msg << endl;
                                    Tango::Except::throw_exception ( (const char *)"Fpga Acquisition",
				            		     msg.c_str(),
						                 (const char *)"AcquisitionThread::run_undetached"); 
                                }

                                row = ch * nofchannels + 2;
                                // memcpy((void *)&(fpga->digital_data_target[ch + 4])[(wf / 4) * samples_per_waveform], (void *)&(fpga->stream_data_target[ch])[(wf * samples_per_waveform) + samples_per_waveform * 2], samples_per_waveform * sizeof(int));
                                if ((retval = SPS_CopyColToShared(fpga->SPS_channels, fpga->wfsps, &(fpga->stream_data_target[ch])[(wf * samples_per_waveform) + samples_per_waveform * 2], fpga->type, row, sizeof(int) * samples_per_waveform, &actual)))
                                {
                                    stop = true;
                                    fpga->acquisition_state = Tango::FAULT;
                                    msg = "SPS_CopyColToShared ch=" + to_string(ch) + " wf=" + to_string(wf) + " failed!";
                                    ERROR_STREAM << msg << endl;
                                    Tango::Except::throw_exception ( (const char *)"Fpga Acquisition",
				            		     msg.c_str(),
						                 (const char *)"AcquisitionThread::run_undetached"); 
                                }
                                // to image type SPS
                                spsimg = const_cast<char*>(fpga->chnames[row].data());
                                if ((retval = SPS_CopyRowToShared(fpga->SPS_scan, spsimg, &(fpga->stream_data_target[ch])[(wf * samples_per_waveform) + samples_per_waveform * 2], fpga->type, fpga->NPTS, sizeof(int) * samples_per_waveform, &actual)))
                                {
                                    stop = true;
                                    fpga->acquisition_state = Tango::FAULT;
                                    msg = "SPS_CopyRowToShared ch=" + to_string(row) + " wf=" + to_string(row) + " failed!";
                                    ERROR_STREAM << msg << endl;
                                    Tango::Except::throw_exception ( (const char *)"Fpga Acquisition",
				            		     msg.c_str(),
						                 (const char *)"AcquisitionThread::run_undetached"); 
                                }

                                row = ch * nofchannels + 3;
                                // memcpy((void *)&(fpga->digital_data_target[ch + 8])[(wf / 4) * samples_per_waveform], (void *)&(fpga->stream_data_target[ch])[(wf * samples_per_waveform) + samples_per_waveform * 3], samples_per_waveform * sizeof(int));
                                if ((retval = SPS_CopyColToShared(fpga->SPS_channels, fpga->wfsps, &(fpga->stream_data_target[ch])[(wf * samples_per_waveform) + samples_per_waveform * 3], fpga->type, row, sizeof(int) * samples_per_waveform, &actual)))
                                {
                                    stop = true;
                                    fpga->acquisition_state = Tango::FAULT;
                                    msg = "SPS_CopyColToShared ch=" + to_string(ch) + " wf=" + to_string(wf) + " failed!";
                                    ERROR_STREAM << msg << endl;
                                    Tango::Except::throw_exception ( (const char *)"Fpga Acquisition",
				            		     msg.c_str(),
						                 (const char *)"AcquisitionThread::run_undetached"); 
                                }
                                // to image type SPS
                                spsimg = const_cast<char*>(fpga->chnames[row].data());
                                if ((retval = SPS_CopyRowToShared(fpga->SPS_scan, spsimg, &(fpga->stream_data_target[ch])[(wf * samples_per_waveform) + samples_per_waveform * 3], fpga->type, fpga->NPTS, sizeof(int) * samples_per_waveform, &actual)))
                                {
                                    stop = true;
                                    fpga->acquisition_state = Tango::FAULT;
                                    msg = "SPS_CopyRowToShared ch=" + to_string(ch) + " wf=" + to_string(wf) + " failed!";
                                    ERROR_STREAM << msg << endl;
                                    Tango::Except::throw_exception ( (const char *)"Fpga Acquisition",
				            		     msg.c_str(),
						                 (const char *)"AcquisitionThread::run_undetached"); 
                                }

                            }

                            samples_rest = samples_in_channel_buffer - (wf * samples_per_waveform);
                            // DEBUG_STREAM << "temp_stream_data_target from stream_data_target " << samples_rest * sizeof(int) << " bytes " << &(fpga->stream_data_target[ch])[wf * samples_per_waveform] << " to " << fpga->temp_stream_data_target + samples_rest << endl;
                            memcpy((void *)fpga->temp_stream_data_target, (void *)&(fpga->stream_data_target[ch])[wf * samples_per_waveform], samples_rest * sizeof(int));
                            // DEBUG_STREAM << "stream_data_target from temp_stream_data_target " << samples_rest * sizeof(int) << " bytes " << fpga->stream_data_target[ch] << " to" << fpga->stream_data_target[ch] + samples_rest << endl;
                            memcpy((void *)fpga->stream_data_target[ch], fpga->temp_stream_data_target, samples_rest * sizeof(int));
                        }
                    }
                    // else // until Jan 2019, pulse_counter_average_en was always 1
                    // {
                    //     waveforms_per_channel = waveforms_in_channel_buffer;
                    //     wf = waveforms_in_channel_buffer;

                    //     //Further separation into 4 real analog channel data and 12 digital virtual channel data
                    //     for (ch = 0; ch < nofchannels; ch++)
                    //     {
                    //         memcpy((void *)&(fpga->analog_data_target[ch])[0], (void *)&(fpga->stream_data_target[ch])[0], wf * samples_per_waveform * sizeof(int));

                    //         samples_rest = samples_in_channel_buffer - (wf * samples_per_waveform);
                    //         memcpy((void *)fpga->temp_stream_data_target, (void *)&(fpga->stream_data_target[ch])[wf * samples_per_waveform], samples_rest * sizeof(int));
                    //         memcpy((void *)fpga->stream_data_target[ch], fpga->temp_stream_data_target, samples_rest * sizeof(int));
                    //     }
                    // }

                    //Set the offset in the buffer to start saving new data from next transfer buffer
                    samples_in_channel_buffer = samples_rest;

                    //Custom data processing can be inserted here.
                    //Completed waveforms are stored in fpga->analog_data_target and fpga->digital_data_target (if, and only if, pulse averaging is enabled).
                    //Number of valid waveforms in each buffer is waveforms_per_channel

                    //At this point each of the 16 target buffers should contain exactly waveforms_per_channel number of complete waveforms
                    //Note that depending on the waveform size, waveforms_per_channel can be larger than 1 due to the alignment of the waveforms in the stream buffer.

                    new_waveforms = min(analog_waveforms_to_collect - collected_waveforms, waveforms_per_channel);

                    DEBUG_STREAM << adq_num << ": Number of waveforms accumulated: " << waveforms_accumulated << endl;
                    //Simple time consumption measurement. For better accuracy use other method.
                    // gettimeofday(&tv, NULL);
                    // time_end_waveform = tv.tv_usec;
                    // time_elapsed_msec = (double)(time_end_waveform - time_start_waveform) / 1000.;
                    time_elapsed_msec = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - time_start_waveform).count();
                    DEBUG_STREAM << adq_num << ": Time consumption for " << waveform_counter - waveform_counter_old << " waveforms: " << time_elapsed_msec << " milliseconds";

                    collected_waveforms = collected_waveforms + new_waveforms;
                    buffer_retry_counter = 0;
                    collect_retry_counter = 0;
                }
                else //if (collect_result)
                {
                    WARN_STREAM << adq_num << ": WARNING: Failed to collect data from DMA buffer, collect_retry_counter = " << collect_retry_counter;
                    collect_retry_counter++;
                    // if (collect_retry_counter > 10000 ) { // Empirical value, just a rather long time, when waiting for fast acq.
                    //     fpga->set_state(Tango::UNKNOWN); // not a DS fault, the trigger didn't come :-(
                    //  break;
                    // }
                }
            }
            else //if (buffers_filled  > 0)
            {
                if (get_logger()->is_warn_enabled())
                {
                    cout << "\r" << adq_num << ": WARNING: Failed to collect data from DMA buffer, buffer_retry_counter = " << buffer_retry_counter;
                }
                buffer_retry_counter++;

                // this is tedious, how long does one want to wait for the trigger ?
                // if (buffer_retry_counter > 100)
                // {
                //     ERROR_STREAM << adq_num << ": ERROR: Failed to collect data from DMA buffer, shutting down.";
                //     buffer_error = 1;
                //     //                  Numbat_WfaShutdown(adq_cu, adq_num);
                //     ShutdownStatus = 1;
                //     fpga->set_state(Tango::FAULT);
                //     break;
                // }
            }
            INFO_STREAM << endl;

            // Check if the target number of waveforms has been collected yet
            if (collected_waveforms >= analog_waveforms_to_collect)
            {
                DEBUG_STREAM << adq_num << ": Target number of waveforms has been reached" << endl;
                DEBUG_STREAM << "Numbat_WfaShutdown ... " << adq_num;
                CHECKADQ(Numbat_WfaShutdown(adq_cu, adq_num));
                DEBUG_STREAM << "passed";
                ShutdownStatus = 1;
            }
            else
            {
                DEBUG_STREAM << adq_num << ": Target number of waveforms NOT reached: " << collected_waveforms << " " << analog_waveforms_to_collect << endl;
                Sleep(100);
            }

// Tyr this out
            }
            catch (Tango::DevFailed &e)
            {
                Tango::Except::print_exception(e);
                omni_mutex_lock l(data_mutex);
                fpga->acquisition_error = e.errors;
                fpga->set_state(Tango::ALARM);
            }
            if (ShutdownStatus == 1)
            {
                do
                {
                    /* This comes from the orignal example program, but is the shutdown
                        * useful, here?
                        */
                    CHECKADQ(Numbat_WfaShutdown(adq_cu, adq_num));
                    CHECKADQ(Numbat_WfaGetStatus(adq_cu, adq_num, NULL, NULL, &in_idle));
                } while (!in_idle);
                DEBUG_STREAM << adq_num << ": Finalized shutdown. End of acquitision while loop" << endl;
                ShutdownStatus = 2;
            }

        } while (ShutdownStatus < 2);

        {
            omni_mutex_lock l(data_mutex);

            // // copy error data after acquisition
            // fpga->dev_error_list.clear();
            // for (unsigned long i = 0; i < dev_error_list.size(); i++)
            // {
            //     fpga->dev_error_list.push_back(dev_error_list[i]);
            // }

            // last thread will write waveforms_accumulated back to the fpga instance
            fpga->waveforms_accumulated = waveforms_accumulated;
            /*
                * Check all threads' acq_fini variable to make sure that the
                * acquisitions are finished. Only when all acquisition loops
                * are finished, can we proceed to autosave !
                * accumulate card index 1 to fpga->n_of_opened_devices
                * mutex protection not needed as every thread has its own
                * acq_fini indexed variable.
                */
            fpga->acq_fini[adq_num] = 0;
            // for (ch = (adq_num - 1); ch < fpga->n_of_opened_devices; ch ++) {
            //     DEBUG_STREAM << "fpga->acq_fini[" << ch <<  "] = " << fpga->acq_fini[ch] << endl;
            // }
            acq_fini = accumulate(fpga->acq_fini.begin(), fpga->acq_fini.end(), 0);
            DEBUG_STREAM << adq_num << ": " << fpga->acq_fini[adq_num] << " and the sum of all : " << acq_fini << endl;
        }

        // when all acq_fini are false, proceed ...
        if (!acq_fini)
        {
            if (fpga->attr_autosave_read[0] && !stop)
            {
                DEBUG_STREAM << "And the state to ON" << endl;
                DEBUG_STREAM << "And SAVE" << endl;
                fpga->set_state(Tango::ON);
                fpga->save();
            }
            else
            {
                DEBUG_STREAM << adq_num << ": Not saving data!" << endl;
            }
        }
        if (stop) {
            // set state to alarm, as something went wrong.
            set_state(Tango::ALARM);
            stop = false;
        }

        DEBUG_STREAM << adq_num << ": END OF ENDLESS LOOP" << endl;
        DEBUG_STREAM << adq_num << "\n\n\n\n\n\n\n\n\n\n" << endl;
    }
    //    this should only happen, when quitting the program
    fpga->set_state(Tango::ON);
    return NULL;
}

} // namespace Fpga_ns
