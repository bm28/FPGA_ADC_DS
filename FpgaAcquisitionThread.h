#ifndef FPGAACQUISITIONTHREAD_H_
#define FPGAACQUISITIONTHREAD_H_

#include <tango.h>
#include <Fpga.h>
#include <algorithm> // std::min

// needed for the rudimentary time measurement and the timeout handling
#include <chrono>  // for high_resolution_clock
typedef std::chrono::high_resolution_clock Time;
typedef std::chrono::milliseconds ms;
typedef std::chrono::duration<float> fsec;

namespace Fpga_ns
{

class FpgaAcquisitionThread : public omni_thread, public Tango::LogAdapter
{
  public:
    FpgaAcquisitionThread(Fpga *_fpga, int _adq_num, omni_mutex &d_m, omni_mutex &g_m);

    // methods needed for common functions between Fpga and this class.
    string get_name();
    inline void set_state(const Tango::DevState &new_state);
    void delete_device();

  private:
    Fpga *fpga;
    int adq_num;
    void *adq_cu;

    omni_mutex &data_mutex;
    omni_mutex &group_mutex;

    void *run_undetached(void *);

    Tango::DevLong updatePeriod;
    std::chrono::time_point<std::chrono::high_resolution_clock> last_update;

    vector<DeviceError> dev_error_list;

    char err[256];
};

} // namespace Fpga_ns

#endif /*FpgaACQUISITIONTHREAD_H_*/
