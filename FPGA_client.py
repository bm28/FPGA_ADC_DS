#!/usr/bin/python


from PyTango import DeviceProxy as Device
import argparse
import os
import time
from subprocess import check_output
from time import sleep

parser = argparse.ArgumentParser(description='Deal with the FPGA device server')
parser.add_argument('name', nargs='?', default="Fpga")

args = parser.parse_args()

PID = check_output(["pidof", args.name])

d = Device("d28/fpga/xmas")


print "Command start"
d.start()

# data = list()
# for i in range(4):
    # data.append([])
# data = d.read()

# now kill the server as the info goes too fast
#sleep(0.5)
#os.kill(PID, 9)
