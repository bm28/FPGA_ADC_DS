
#define _CRT_SECURE_NO_WARNINGS
#define COMPATIBLE_NUMBAT_FW 38448
#include "ADQAPI.h"
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#ifdef LINUX
  #include <stdlib.h>
  #include <string.h>
  #include <unistd.h>
  #define _kbhit() kbhit_linux()
  #define Sleep(a) usleep(1000*a)
#else
  #include <conio.h>
  #define __func__ __FUNCTION__
#endif

#ifdef _DEBUG
  #define scanf(a, b) noscanf()
  //#define printf(A, ...) ADQControlUnit_UserLogMessage(adq_cu, 0x4, A, ##__VA_ARGS__)
  //#define printf(A, ...) my_printf(adq_cu, A, ##__VA_ARGS__)
#endif


#ifndef __Numbat_H
#define __Numbat_H


//#define CHECKADQ(f) if(!(f)){printf("ERROR: In " #f "\n"); last_error = 1; goto error;}
//#define CHECKEXIT(f) if(!(f)){printf("ERROR: In " #f "\n"); last_error = 1;}
#define MIN(a,b) ((a) > (b) ? (b) : (a))
#define MAX(a,b) ((a) > (b) ? (a) : (b))

#define USRREG_READ_BASE                     0x1A
#define USRREG_WRITE_BASE                    0x10

//Bit position for WFA_CONTROL_ADDR register
#define WFA_ARM_POS                          0u
#define WFA_BYPASS_INV_POS                   1u
#define WFA_AUTO_REARM_POS                   2u
#define WFA_IMMEDIATE_READOUT_POS            3u
#define WFA_READOUT_POS                      4u
#define WFA_SCHEDULE_SHUTDOWN_POS            5u
#define WFA_WAVEFORM_PER_TRIGGER_POS         6u
#define WFA_TRIGGER_COUNT_MODE_POS           7u
#define PULSE_AVERAGE_ENABLE_POS             29u
#define TRIGGER_TEST_POS                     30u
#define TRIGGER_EDGE_POS                     31u

#define WFA_ARM_BIT                          1u
#define WFA_BYPASS_INV_BIT                   1u << WFA_BYPASS_INV_POS
#define WFA_AUTO_REARM_BIT                   1u << WFA_AUTO_REARM_POS
#define WFA_IMMEDIATE_READOUT_BIT            1u << WFA_IMMEDIATE_READOUT_POS
#define WFA_READOUT_BIT                      1u << WFA_READOUT_POS
#define WFA_SCHEDULE_SHUTDOWN_BIT            1u << WFA_SCHEDULE_SHUTDOWN_POS
#define WFA_WAVEFORM_PER_TRIGGER_BIT         1u << WFA_WAVEFORM_PER_TRIGGER_POS
#define WFA_TRIGGER_COUNT_MODE_BIT           1u << WFA_TRIGGER_COUNT_MODE_POS
#define TRIGGER_EDGE_BIT                     1u << TRIGGER_EDGE_POS
#define TRIGGER_TEST_BIT                     1u << TRIGGER_TEST_POS
#define PULSE_AVERAGE_ENABLE_BIT             1u << PULSE_AVERAGE_ENABLE_POS

#define NORMAL_READOUT                       0
#define IMMEDIATE_READOUT                    1

#define MANUAL_REARM                         0
#define AUTO_REARM                           1

#define SINGLE_TRIGGER_NO_DEAD_TIME          0
#define MULTI_TRIGGER_WAVEFORM_PER_TRIGGER   1

//Writable registers
#define MOVING_AVERAGE_CONTROL_ADDR          USRREG_WRITE_BASE+0
#define TIMESTEP_CONTROL_ADDR                USRREG_WRITE_BASE+1
#define WFA_CONTROL_ADDR                     USRREG_WRITE_BASE+2
#define WFA_NOF_SAMPLESCYCLES_ADDR           USRREG_WRITE_BASE+3
#define WFA_NOF_WAVEFORMS_ADDR               USRREG_WRITE_BASE+4
#define WFA_NOF_PRETRIGGERCYCLES_ADDR        USRREG_WRITE_BASE+5
#define WFA_NOF_HOLDOFFCYCLES_ADDR           USRREG_WRITE_BASE+6
#define WFA_NOF_READOUTWAITCYCLES_ADDR       USRREG_WRITE_BASE+7
#define WFA_NOF_TRIGGERS_LIMIT_ADDR          USRREG_WRITE_BASE+8
#define DEBUG_PULSE_GENERATOR_ADDR           USRREG_WRITE_BASE+9

//Read only registers
#define ANALOG_AVERAGE_STATUS_ADDR           USRREG_READ_BASE+0
#define DIGITAL_AVERAGE_STATUS_ADDR          USRREG_READ_BASE+1
#define ANALOG_WAVEFORMS_COLLECTED_ADDR      USRREG_READ_BASE+2
#define DIGITAL_WAVEFORMS_COLLECTED_ADDR     USRREG_READ_BASE+3

//Trigger modes constants
#define SET_SW_TRIGGER_MODE                  1
#define SET_EXT_TRIGGER_MODE                 2
#define SET_LEVEL_TRIGGER_MODE               3
#define SET_INTERNAL_TRIGGER_MODE            4

#define TIMESTEP_RESOLUTION_POS              0u
#define TIMESTEP_MULTIPLIER_POS              5u
#define TIMESTEP_SYNC_MODE_POS               31u

//Bit position for debug pulse generator that is also used to chose gpio pin
#define PULSE_PERIOD_CYCLES_POS              0u
#define PULSES_PER_TRAIN_POS                 16u
#define TRIGGER_EVENT_SELECT_POS             29u
#define PULSE_TRAIN_MODE_POS                 30u
#define PULSE_GEN_ONOFF_POS                  31u

//Bit position for overflow flags
#define STREAMING_OVERFLOW_POS               0u     // 0 = Global Device Streaming Overflow Flag
#define ANALOG_OUTPUT_FIFO_OVERFLOW_POS      1u     // 1 = Analog Waveform Average Output Fifo Overflow
#define ANALOG_MA_OVERFLOW_POS               2u     // 2 = Analog Moving Average Bit Overflow (Accumulation sum exceeds 23 bits)
#define ANALOG_WFA_OVERFLOW_POS              3u     // 3 = Analog Waveform Average Accumulation Overflow (Accumulation sum exceeds 32 bits)
#define DIGITAL_OUTPUT_FIFO_OVERFLOW_POS     4u     // 4 = Digital Waveform Average Output Fifo Overflow
#define DIGITAL_SCHEDULE_FIFO_OVERFLOW_POS   5u     // 5 = Digital Waveform Average Schedule Fifo Overflow
#define DIGITAL_WFA_OVERFLOW_POS             6u     // 6 = Digital Waveform Average Accumulation Overflow (Accumulation sum exceeds 32 bits)

#define STREAMING_OVERFLOW_BIT               1u << STREAMING_OVERFLOW_POS               // 0 = Global Device Streaming Overflow Flag
#define ANALOG_OUTPUT_FIFO_OVERFLOW_BIT      1u << ANALOG_OUTPUT_FIFO_OVERFLOW_POS      // 1 = Analog Waveform Average Output Fifo Overflow
#define ANALOG_MA_OVERFLOW_BIT               1u << ANALOG_MA_OVERFLOW_POS               // 2 = Analog Moving Average Bit Overflow (Accumulation sum exceeds 23 bits)
#define ANALOG_WFA_OVERFLOW_BIT              1u << ANALOG_WFA_OVERFLOW_POS              // 3 = Analog Waveform Average Accumulation Overflow (Accumulation sum exceeds 32 bits)
#define DIGITAL_OUTPUT_FIFO_OVERFLOW_BIT     1u << DIGITAL_OUTPUT_FIFO_OVERFLOW_POS     // 4 = Digital Waveform Average Output Fifo Overflow
#define DIGITAL_SCHEDULE_FIFO_OVERFLOW_BIT   1u << DIGITAL_SCHEDULE_FIFO_OVERFLOW_POS   // 5 = Digital Waveform Average Schedule Fifo Overflow
#define DIGITAL_WFA_OVERFLOW_BIT             1u << DIGITAL_WFA_OVERFLOW_POS             // 6 = Digital Waveform Average Accumulation Overflow (Accumulation sum exceeds 32 bits)


////Bit position for GPIO
//#define GPIO_PIN_0                           0u
//#define GPIO_PIN_1                           1u
//#define GPIO_PIN_2                           2u
//#define GPIO_PIN_3                           3u
//#define GPIO_PIN_4                           4u
//#define GPIO_PIN_5                           5u
//#define GPIO_PIN_6                           6u
//#define GPIO_PIN_7                           7u
//#define GPIO_PIN_8                           8u
//#define GPIO_PIN_9                           9u
//#define GPIO_PIN_10                          10u
//#define GPIO_PIN_11                          11u
//#define GPIO_PIN_12                          12u
//#define GPIO_PIN_13                          13u
//#define GPIO_PIN_14                          14u
//#define GPIO_PIN_15                          15u
//#define TRIGGER_EVENT                        16u

// void ManualReArmExample(void *adq_cu, int adq_num);
void ManualReArmExample(void *adq_cu, int adq_num, int argc, char** argv);
void AutoReArmExample(void *adq_cu, int adq_num);

int kbhit_linux (void);

//User register controlled functions
unsigned int Numbat_SetMovingAverageSize(void* adq_cu, int adq_num, unsigned int framesize_in_samples);
unsigned int Numbat_SetTimeStepSize(void* adq_cu, int adq_num, unsigned int timestep_sync_mode, unsigned int multiplier, unsigned int resolution_in_ns);
unsigned int Numbat_SetPulseCounterAveragingMode(void* adq_cu, int adq_num, unsigned int mode);
unsigned int Numbat_WfaSetup(void* adq_cu, 
                          int adq_num,
                          unsigned int NofWaveforms, 
                          unsigned int NofSamples,
                          unsigned int NofPreTriggerSamples, 
                          unsigned int NofHoldOffSamples,
                          unsigned int triggers_limit, 
                          unsigned int trigger_count_mode, 
                          unsigned int trigger_edge, 
                          unsigned int pulse_average_mode, 
                          unsigned int ArmMode, 
                          unsigned int ReadoutMode, 
                          unsigned int AccMode);

unsigned int Numbat_SoftwareTrigger(void* adq_cu, int adq_num);
unsigned int Numbat_WfaSetTriggerEdge(void* adq_cu, int adq_num, unsigned int trigger_edge);
unsigned int Numbat_WfaAccMode(void* adq_cu, int adq_num, unsigned int accumulation_mode);
unsigned int Numbat_WfaSetNumberOfTriggers(void* adq_cu, int adq_num, unsigned int triggers_limit);
unsigned int Numbat_WfaSetTriggerCountMode(void* adq_cu, int adq_num, unsigned int trigger_count_mode);
unsigned int Numbat_WfaSetArmMode(void* adq_cu, int adq_num, unsigned int arm_mode);
unsigned int Numbat_WfaArm(void* adq_cu, int adq_num);
unsigned int Numbat_ParseDataStream(unsigned int samples_per_record, int* data_stream, int** data_target);
unsigned int Numbat_WfaShutdown(void* adq_cu, int adq_num);
unsigned int Numbat_WfaGetStatus(void* adq_cu, int adq_num, unsigned int* data_available, unsigned int* waveforms_accumulated, unsigned int* in_idle);
unsigned int Numbat_WfaGetWaveform(void* adq_cu, int adq_num);
unsigned int Numbat_WfaDisarm(void* adq_cu, int adq_num);
unsigned int Numbat_FlushData(void* adq_cu, int adq_num);
unsigned int Numbat_ResetUserRegisters(void* adq_cu, int adq_num);
unsigned int Numbat_GetStreamOverflow(void* adq_cu, int adq_num, unsigned int* overflow);
unsigned int Numbat_SetupDebugPulseGen(void* adq_cu, int adq_num, unsigned int debug_pulse_select, unsigned int train_mode, unsigned int pulses_per_train, unsigned int pulse_period_cycles);
unsigned int Numbat_SelectGPIO(void* adq_cu, int adq_num, unsigned int gpio_pin);
void noscanf();

#endif
