#include "Sps/Include/sps.h"
#include "Sps/Include/spec_shm.h"
#include <unistd.h>
#include <iostream>
#include <cstdlib>
#include <string>
#include <vector>

#include <math.h> /* sin */

// using namespace std;

int main(int argc, char *argv[])
{

    char *spec_version = argv[1];
    char *arrayname    = argv[2];
    int rows, cols, type, flag, c, r, x;
    int *ptr;
    SPS_GetArrayInfo(spec_version, arrayname, &rows, &cols, &type, &flag);

    std::cout << "cols " << cols << " rows " << rows << " type " << type << " flags " << flag<< std::endl;

    char *infostr;
    char *metastr;

    infostr = SPS_GetInfoString(spec_version, arrayname);
    std::cout << "info " << infostr << std::endl;
    void * spsptr;
    spsptr = SPS_GetDataPointer(spec_version, arrayname, 0);

    // for (x = 0; x < 4096; x++) {
    //     if (!(x % 16)) std::cout << std::endl;
    //     std::cout << *((double *)spsptr + x) << " " << std::flush ;
    // }
    // for (r = 0; r < 100; r++)
    // {
    //     std::cout << "col " << r << std::endl;
    //     for (c = 0; c < cols; c++)
    //     {
    //         std::cout << *((double *)spsptr + r * cols + c) << " " << std::flush ;
    //     }
    //     std::cout << "\n" << std::endl;
    // }

    u32_t length;
    metastr = SPS_GetMetaData(spec_version, arrayname, &length);
    std::cout << "meta " << metastr << std::endl;

    char *envkeystr;
    std::string xxx = arrayname + "_ENV";
    std::cout << xxx << std::endl;
    envkeystr = SPS_GetNextEnvKey(spec_version, xxx.c_str(), 0);
}
