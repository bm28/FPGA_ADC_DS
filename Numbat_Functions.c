// 2011 (C)opyright Signal Processing Devices Sweden AB

#include "Numbat.h"
#include <tango.h>
#include "Fpga.h"
#include "FpgaAcquisitionThread.h"
#include "FpgaClass.h"
#include <iostream>

int kbhit_linux(void)
{
#ifndef LINUX
    return 1;
#else
    struct timeval tv;
    fd_set rdfs;

    tv.tv_sec = 0;
    tv.tv_usec = 0;

    FD_ZERO(&rdfs);
    FD_SET(STDIN_FILENO, &rdfs);

    select(STDIN_FILENO + 1, &rdfs, NULL, NULL, &tv);
    return FD_ISSET(STDIN_FILENO, &rdfs);
#endif
}

unsigned int Numbat_SoftwareTrigger(void *adq_cu, int adq_num)
{
    unsigned int success;
    success = ADQ_WriteUserRegister(adq_cu, adq_num, 2, WFA_CONTROL_ADDR, ~(TRIGGER_TEST_BIT), TRIGGER_TEST_BIT, NULL);
    Sleep(1);
    success = success && ADQ_WriteUserRegister(adq_cu, adq_num, 2, WFA_CONTROL_ADDR, ~(TRIGGER_TEST_BIT), ~(TRIGGER_TEST_BIT), NULL);
    return success;
}

unsigned int Numbat_WfaSetTriggerEdge(void *adq_cu, int adq_num, unsigned int trigger_edge)
{
    unsigned int success;
    success = ADQ_WriteUserRegister(adq_cu, adq_num, 2, WFA_CONTROL_ADDR, ~(TRIGGER_EDGE_BIT), trigger_edge << TRIGGER_EDGE_POS, NULL);
    return success;
}

unsigned int Numbat_WfaSetNumberOfTriggers(void *adq_cu, int adq_num, unsigned int triggers_limit)
{
    unsigned int success;

    success = ADQ_WriteUserRegister(adq_cu, adq_num, 2, WFA_NOF_TRIGGERS_LIMIT_ADDR, 0, triggers_limit, NULL);

    return success;
}

unsigned int Numbat_WfaSetTriggerCountMode(void *adq_cu, int adq_num, unsigned int trigger_count_mode)
{
    unsigned int success;

    success = ADQ_WriteUserRegister(adq_cu, adq_num, 2, WFA_CONTROL_ADDR, ~(WFA_TRIGGER_COUNT_MODE_BIT), (trigger_count_mode << WFA_TRIGGER_COUNT_MODE_POS), NULL);

    return success;
}

unsigned int Numbat_WfaSetArmMode(void *adq_cu, int adq_num, unsigned int arm_mode)
{
    unsigned int success;
    if ((arm_mode == 0) || (arm_mode == 1))
    {
        success = ADQ_WriteUserRegister(adq_cu, adq_num, 2, WFA_CONTROL_ADDR, ~(WFA_AUTO_REARM_BIT), arm_mode << WFA_AUTO_REARM_POS, NULL);
    }
    else
        success = 0;

    return success;
}

unsigned int Numbat_WfaSetReadoutMode(void *adq_cu, int adq_num, unsigned int readout_mode)
{
    unsigned int success;
    if ((readout_mode == 0) || (readout_mode == 1))
    {
        success = ADQ_WriteUserRegister(adq_cu, adq_num, 2, WFA_CONTROL_ADDR, ~(WFA_IMMEDIATE_READOUT_BIT), readout_mode << WFA_IMMEDIATE_READOUT_POS, NULL);
    }
    else
        success = 0;

    return success;
}

unsigned int Numbat_WfaSetReadoutWaitCycles(void *adq_cu, int adq_num, unsigned int wait_cycles)
{
    unsigned int success;
    success = ADQ_WriteUserRegister(adq_cu, adq_num, 2, WFA_NOF_READOUTWAITCYCLES_ADDR, 0, wait_cycles, NULL);

    return success;
}

unsigned int Numbat_WfaAccMode(void *adq_cu, int adq_num, unsigned int accumulation_mode)
{
    unsigned int success;

    if (accumulation_mode > 1)
    {
        std::cerr << "ERROR: Invalid Accumulation mode " << accumulation_mode << "! (Valid value: 0 or 1)" << endl;
        return 0;
    }

    success = ADQ_WriteUserRegister(adq_cu, adq_num, 2, WFA_CONTROL_ADDR, ~(WFA_WAVEFORM_PER_TRIGGER_BIT), accumulation_mode << WFA_WAVEFORM_PER_TRIGGER_POS, NULL);

    return success;
}

unsigned int Numbat_SetMovingAverageSize(void *adq_cu, int adq_num, unsigned int framesize_in_samples)
{
    unsigned int success;

    //max frame size for moving average is 128 samples
    if (framesize_in_samples > 128)
    {
        std::cerr << "ERROR: Moving average framsize (" << framesize_in_samples << " samples) exceed max value (128 samples)!" << endl;
        success = 0;
    }
    else
    {
        //Set framesize in number of samples for the moving average. Note the register mask 0xFFFFFF80 used to leave the MSB unaffected!
        success = ADQ_WriteUserRegister(adq_cu, adq_num, 2, MOVING_AVERAGE_CONTROL_ADDR, 0xFFFFFF80, framesize_in_samples, NULL);
    }

    return success;
}

unsigned int Numbat_SetTimeStepSize(void *adq_cu, int adq_num, unsigned int timestep_sync_mode, unsigned int multiplier, unsigned int resolution_in_ns)
{
    unsigned int success = 1;
    unsigned int timestep_control_value;
    //Check for valid paramters
    //Smalest resolution is 25 ns and largest is 64 ns. But 0 ns is also a valid parameter and it means that no timestep is used.
    //So check for invalid resolution between 0 and 25 ns
    //The truth is that the resolution can be as smal as 6 ns. But the pulse counter will not work for such smal time frame. It needs at least 16 ns.
    if ((resolution_in_ns < 25) && (resolution_in_ns != 0))
    {
        std::cerr << "ERROR: Timestep resolution " << resolution_in_ns << " is invalid!\n(Valid value: 0, 25, 26, 27...32)" << endl;
        success = 0;
    }

    if (resolution_in_ns > 32) //5 bits value 2^5 = 32
    {
        std::cerr << "ERROR: Timestep resolution " << resolution_in_ns << " is invalid! (Valid value: 0, 25, 26, 27...32)" << endl;
        success = 0;
    }

    if (multiplier > 134217728) //27 bits value 2^27 = 134217728
    {
        std::cerr << "ERROR: Timestep resolution multiplier " << multiplier << " is invalid! (Valid value: 0..134217728 (2^27)" << endl;
        success = 0;
    }

    if (timestep_sync_mode > 1)
    {
        std::cerr << "ERROR: Timestep Sync mode " << timestep_sync_mode << " is invalid! (Valid value: 0 or 1)" << endl;
        success = 0;
    }

    if (success)
    {
        //Put both parameters into the same register
        timestep_control_value = (multiplier << TIMESTEP_MULTIPLIER_POS) + resolution_in_ns;

        //Set the timestep based on resolution and and multiplier. The resulting timestep = resolution*multiplier.
        //If the product resolution*multiplier is zero, it means that no timestep between samples is used.
        success = ADQ_WriteUserRegister(adq_cu, adq_num, 2, TIMESTEP_CONTROL_ADDR, 0, timestep_control_value, NULL);

        //Note that the sync mode bit is borrowed from the MOVING_AVERAGE_CONTROL register!!!! because the bit length for TIMESTEP_CONTROL_ADDR is not enough for 27 bits multiplier.
        success = success && ADQ_WriteUserRegister(adq_cu, adq_num, 2, MOVING_AVERAGE_CONTROL_ADDR, ~(1u << TIMESTEP_SYNC_MODE_POS), (timestep_sync_mode << TIMESTEP_SYNC_MODE_POS), NULL);
    }

    return success;
}

unsigned int Numbat_SetPulseCounterAveragingMode(void *adq_cu, int adq_num, unsigned int mode)
{
    unsigned int success;
    //0 = disable, 1 = enable
    success = ADQ_WriteUserRegister(adq_cu, adq_num, 2, WFA_CONTROL_ADDR, ~(PULSE_AVERAGE_ENABLE_BIT), mode << PULSE_AVERAGE_ENABLE_POS, NULL);

    return success;
}

unsigned int Numbat_WfaSetup(void *adq_cu,
                             int adq_num,
                             unsigned int NofWaveforms,
                             unsigned int NofSamples,
                             unsigned int NofPreTriggerSamples,
                             unsigned int NofHoldOffSamples,
                             unsigned int triggers_limit,
                             unsigned int trigger_count_mode,
                             unsigned int trigger_edge,
                             unsigned int pulse_average_mode,
                             unsigned int ArmMode,
                             unsigned int ReadoutMode,
                             unsigned int AccMode)
{
    unsigned int success = 1;
    unsigned int parallel_input_samples = 2;

    unsigned int nofwaveforms = NofWaveforms;
    unsigned int sample_cycles = NofSamples / parallel_input_samples;
    unsigned int pretrigger_cycles = NofPreTriggerSamples / parallel_input_samples;
    unsigned int holdoff_cycles = NofHoldOffSamples / parallel_input_samples;
    unsigned int readoutwait_cycles;

    success = success && ADQ_WriteUserRegister(adq_cu, adq_num, 2, WFA_CONTROL_ADDR, 0, 0, NULL); //Reset WFA control register

    // Make error checks
    //Use this check when the amount of triggers are limited to a specific limit
    //if (NofSamples % 8 > 0)    //128 bytes packet size on pcie/USB3? 4 bytes * 8 samples * 4 channels = 128 bytes

    if (NofSamples % 2 > 0) //Use this check if the triggers are free running and has no limit of how mny times it fires
    {
        std::cerr << "ERROR: Waveform length for should be in steps of 8 samples!\n"
                     << endl;
        return 0;
    }

    readoutwait_cycles = 450; //This value does not have any effect if IMMEDIATE_READOUT mode is used.

    // Check maximum size of waveform counter. 2^32 = 4294967296
    // Accumulation overflow might occur even for lower values, depending on the signal level and moving average frame size
	if ((nofwaveforms < 1) || (nofwaveforms >= 4294967296 -1)) {
//      if (nofwaveforms < 1 || nofwaveforms > 4294967296) { // original was 2^32 and not 2^32 - 1
        std::cerr << "ERROR: Number of waveform is greater than 2^25.\n"
                     << endl;
        return 0;
    }

    // Check maximum size of Block RAM for averaging. 1024 cycles * 2 parallel samples = 2048 samples
    if (sample_cycles < 1 || sample_cycles > 1024)
    {
        std::cerr << "ERROR: Waveform length should not exceed 1024*2 = 2048 samples.\n"
                     << endl;
        return 0;
    }

    // Check maximum size of pretrigger FIFO
    if (pretrigger_cycles > 256)
    {
        std::cerr << "ERROR: Pretrigger length should not exceed 256*2 = 512 samples.\n"
                     << endl;
        return 0;
    }

    // Check maximum size of holdoff counter. 256 cycles * 2 samples = 512 samples
    if (holdoff_cycles > 256)
    {
        std::cerr << "ERROR: holdoff length should not exceed 256*2 = 512 samples.\n"
                     << endl;
        return 0;
    }

    if ((trigger_edge != 0) && (trigger_edge != 1))
    {
        std::cerr << "ERROR: Trigger edge must be either 0 = Negative Flank or 1 = Positive Flank.\n"
                     << endl;
        return 0;
    }

    if (ArmMode == MANUAL_REARM) //Manual re-arm mode
    {
        if (ReadoutMode == IMMEDIATE_READOUT)
        {
            std::cerr << "ERROR: Immediate readout mode is only available in auto re-arm mode!\n"
                         << endl;
            return 0;
        }
    }
    else if (ArmMode == AUTO_REARM)
    {
        Numbat_WfaSetReadoutMode(adq_cu, adq_num, ReadoutMode);
    }
    else
    {
        std::cerr << "ERROR: Invalid Arm mode! (Either 0 or 1 can be used)\n"
                     << endl;
        return 0;
    }

    success = success && Numbat_WfaAccMode(adq_cu, adq_num, AccMode);
    success = success && Numbat_WfaSetNumberOfTriggers(adq_cu, adq_num, triggers_limit);
    success = success && Numbat_WfaSetTriggerCountMode(adq_cu, adq_num, trigger_count_mode);
    success = success && Numbat_SetPulseCounterAveragingMode(adq_cu, adq_num, pulse_average_mode);
    success = success && Numbat_WfaSetTriggerEdge(adq_cu, adq_num, trigger_edge);
    success = success && Numbat_WfaSetArmMode(adq_cu, adq_num, ArmMode);

    //Set paramters
    success = success && ADQ_WriteUserRegister(adq_cu, adq_num, 2, WFA_NOF_SAMPLESCYCLES_ADDR, 0, sample_cycles, NULL);
    success = success && ADQ_WriteUserRegister(adq_cu, adq_num, 2, WFA_NOF_WAVEFORMS_ADDR, 0, nofwaveforms, NULL);
    success = success && ADQ_WriteUserRegister(adq_cu, adq_num, 2, WFA_NOF_PRETRIGGERCYCLES_ADDR, 0, pretrigger_cycles, NULL);
    success = success && ADQ_WriteUserRegister(adq_cu, adq_num, 2, WFA_NOF_HOLDOFFCYCLES_ADDR, 0, holdoff_cycles, NULL);
    success = success && ADQ_WriteUserRegister(adq_cu, adq_num, 2, WFA_NOF_READOUTWAITCYCLES_ADDR, 0, readoutwait_cycles, NULL);

    // Put in non-bypass mode
    success = success && ADQ_WriteUserRegister(adq_cu, adq_num, 2, WFA_CONTROL_ADDR, ~(WFA_BYPASS_INV_BIT), WFA_BYPASS_INV_BIT, NULL); // Put in non-bypass mode

    return success;
}

unsigned int Numbat_WfaArm(void *adq_cu, int adq_num)
{
    unsigned int success = 1;

    success = success && ADQ_WriteUserRegister(adq_cu, adq_num, 2, WFA_CONTROL_ADDR, ~(WFA_ARM_BIT), ~(WFA_ARM_BIT), NULL);            //Make sure it is disarmed first
    success = success && ADQ_WriteUserRegister(adq_cu, adq_num, 2, WFA_CONTROL_ADDR, ~(WFA_BYPASS_INV_BIT), WFA_BYPASS_INV_BIT, NULL); // Put in non-bypass mode

    success = success && ADQ_WriteUserRegister(adq_cu, adq_num, 2, WFA_CONTROL_ADDR, ~(WFA_ARM_BIT), WFA_ARM_BIT, NULL); //Arm

    return success;
}

unsigned int Numbat_ParseDataStream(unsigned int samples_per_waveform, int *data_stream, int **data_target)
{
    //Insert parsing function here if data needs to be resorted into som other order.

    return 1;
}

unsigned int Numbat_WfaShutdown(void *adq_cu, int adq_num)
{
    unsigned int success;

    success = ADQ_WriteUserRegister(adq_cu, adq_num, 2, WFA_CONTROL_ADDR, ~(WFA_SCHEDULE_SHUTDOWN_BIT), WFA_SCHEDULE_SHUTDOWN_BIT, NULL);
    success = success && ADQ_SetTriggerMode(adq_cu, adq_num, 1); //Restore trigger mode

    return success;
}

unsigned int Numbat_WfaGetStatus(void *adq_cu, int adq_num, unsigned int *data_available, unsigned int *waveforms_accumulated, unsigned int *in_idle)
{
    unsigned int status_reg;
    unsigned int analog_status_reg;
    unsigned int digital_status_reg;
    unsigned int success;

    success = ADQ_ReadUserRegister(adq_cu, adq_num, 2, ANALOG_AVERAGE_STATUS_ADDR, &analog_status_reg);

    success = success && ADQ_ReadUserRegister(adq_cu, adq_num, 2, DIGITAL_AVERAGE_STATUS_ADDR, &digital_status_reg);

    status_reg = analog_status_reg & digital_status_reg; //bitwise and to check status for both

    if (success)
    {
        if (data_available != NULL)
        {
            *data_available = status_reg & 0x01;
        }

        if (in_idle != NULL)
        {
            *in_idle = status_reg & 0x02;
        }

        if (waveforms_accumulated != NULL)
        {
            success = ADQ_ReadUserRegister(adq_cu, adq_num, 2, ANALOG_WAVEFORMS_COLLECTED_ADDR, waveforms_accumulated);
        }
    }

    return success;
}

unsigned int Numbat_WfaGetWaveform(void *adq_cu, int adq_num)
{
    unsigned int success = 1;

    success = success && ADQ_WriteUserRegister(adq_cu, adq_num, 2, WFA_CONTROL_ADDR, ~(WFA_READOUT_BIT), WFA_READOUT_BIT, NULL);
    success = success && ADQ_WriteUserRegister(adq_cu, adq_num, 2, WFA_CONTROL_ADDR, ~(WFA_READOUT_BIT), ~(WFA_READOUT_BIT), NULL);

    return success;
}

unsigned int Numbat_WfaDisarm(void *adq_cu, int adq_num)
{
    unsigned int success;
    success = ADQ_WriteUserRegister(adq_cu, adq_num, 2, WFA_CONTROL_ADDR, ~(WFA_ARM_BIT), ~(WFA_ARM_BIT), NULL);                        //Make sure it is disarmed first
    success = success && ADQ_WriteUserRegister(adq_cu, adq_num, 2, WFA_CONTROL_ADDR, ~(WFA_BYPASS_INV_BIT), ~WFA_BYPASS_INV_BIT, NULL); // Turn on bypass mode (data pass throu)
    return success;
}

unsigned int Numbat_FlushData(void *adq_cu, int adq_num)
{
    unsigned int success;
    unsigned int in_idle;
    unsigned int analog_status_reg;
    unsigned int digital_status_reg;
    unsigned int analog_transfer_in_progress = 1;
    unsigned int digital_transfer_in_progress = 1;

    //This loop checks if the transfer of both the analog and digital channels has finished before flushing out the data.
    //This is a better solution than letting the user manually wait for transfer to finish by using Sleep e.t.c. Because they might forget to do it.
    do
    {
        //Read the whole status register
        success = ADQ_ReadUserRegister(adq_cu, adq_num, 2, ANALOG_AVERAGE_STATUS_ADDR, &analog_status_reg);
        success = success & ADQ_ReadUserRegister(adq_cu, adq_num, 2, DIGITAL_AVERAGE_STATUS_ADDR, &digital_status_reg);

        //Mask out the transfer in progress bit
        analog_transfer_in_progress = (analog_status_reg >> 5) & 0x1;
        digital_transfer_in_progress = (digital_status_reg >> 5) & 0x1;

        // if (analog_transfer_in_progress || digital_transfer_in_progress)
        // {
        //     std::cout << "\rTransfer in progress..." << endl;
        // }

    } while (analog_transfer_in_progress || digital_transfer_in_progress);

    // std::cout << "\n"
    //              << endl;

    //Exit state machine gracefully but still stay in non-bypass mode
    do
    {
        success = Numbat_WfaShutdown(adq_cu, adq_num);
        success = success && Numbat_WfaGetStatus(adq_cu, adq_num, NULL, NULL, &in_idle);
    } while (!in_idle);

    success = success && ADQ_WriteUserRegister(adq_cu, adq_num, 2, WFA_CONTROL_ADDR, ~(WFA_BYPASS_INV_BIT), ~WFA_BYPASS_INV_BIT, NULL); // Briefly turn on bypass mode (data pass throu)
    Sleep(1);
    success = success && ADQ_WriteUserRegister(adq_cu, adq_num, 2, WFA_CONTROL_ADDR, ~(WFA_BYPASS_INV_BIT), WFA_BYPASS_INV_BIT, NULL); // Put in non-bypass mode again to prevent excessive data flow
    return success;
}

unsigned int Numbat_ResetUserRegisters(void *adq_cu, int adq_num)
{
    unsigned int success = 1;
    //Reset all user write register to zero
    unsigned int user_reg;
    for (user_reg = 0; user_reg < 10; user_reg++)
    {
        success = success && ADQ_WriteUserRegister(adq_cu, adq_num, 2, USRREG_WRITE_BASE + user_reg, 0, 0, NULL);
    }

    return success;
}

unsigned int Numbat_GetStreamOverflow(void *adq_cu, int adq_num, unsigned int *overflow)
{

    unsigned int streaming_fifo_overflow;
    unsigned int analog_status_reg;
    unsigned int digital_status_reg;
    unsigned int success;

    success = ADQ_ReadUserRegister(adq_cu, adq_num, 2, ANALOG_AVERAGE_STATUS_ADDR, &analog_status_reg);
    success = success & ADQ_ReadUserRegister(adq_cu, adq_num, 2, DIGITAL_AVERAGE_STATUS_ADDR, &digital_status_reg);

    //Mask out the overflow bits only.
    analog_status_reg = (analog_status_reg >> 2u) & 0x7;
    digital_status_reg = (digital_status_reg >> 2u) & 0x7;

    //Align the status flag to create a bitfield
    analog_status_reg = analog_status_reg << 1u;
    digital_status_reg = digital_status_reg << 4u;

    //Get the global streaming overflow flag
    streaming_fifo_overflow = ADQ_GetStreamOverflow(adq_cu, adq_num);

    if (success)
    {
        //Create a unique overflow value of the combined flags.
        *overflow = digital_status_reg + analog_status_reg + streaming_fifo_overflow;
    }

    //Bit position meaning:
    // 0 = Global Device Streaming Overflow Flag
    // 1 = Analog Waveform Average Output Fifo Overflow
    // 2 = Analog Moving Average Bit Overflow (Accumulation sum exceeds 23 bits)
    // 3 = Analog Waveform Average Accumulation Overflow (Accumulation sum exceeds 32 bits)
    // 4 = Digital Waveform Average Output Fifo Overflow
    // 5 = Digital Waveform Average Schedule Fifo Overflow
    // 6 = Digital Waveform Average Accumulation Overflow (Accumulation sum exceeds 32 bits)

    if (*overflow & STREAMING_OVERFLOW_BIT)
        std::cerr << "\nERROR: Streaming Overflow!\n\n"
                     << endl;

    if (*overflow & ANALOG_OUTPUT_FIFO_OVERFLOW_BIT)
        std::cerr << "\nERROR: Analog Waveform Average Output Fifo Overflow!\n\n"
                     << endl;

    if (*overflow & ANALOG_MA_OVERFLOW_BIT)
        std::cerr << "\nERROR: Analog Moving Average Bit Overflow!\n(Accumulation sum exceeds 23 bits)\n\n"
                     << endl;

    if (*overflow & ANALOG_WFA_OVERFLOW_BIT)
        std::cerr << "\nERROR: Analog Waveform Average Accumulation Overflow!\n(Accumulation sum exceeds 32 bits)\n\n"
                     << endl;

    if (*overflow & DIGITAL_OUTPUT_FIFO_OVERFLOW_BIT)
        std::cerr << "\nERROR: Digital Waveform Average Output Fifo Overflow\n\n"
                     << endl;

    if (*overflow & DIGITAL_SCHEDULE_FIFO_OVERFLOW_BIT)
        std::cerr << "\nERROR: Digital Waveform Average Schedule Fifo Overflow\n\n"
                     << endl;

    if (*overflow & DIGITAL_WFA_OVERFLOW_BIT)
        std::cerr << "\nERROR: Digital Waveform Average Accumulation Overflow!\n(Accumulation sum exceeds 32 bits)\n\n"
                     << endl;

    return success;
}

unsigned int Numbat_SetupDebugPulseGen(void *adq_cu, int adq_num, unsigned int debug_pulse_select, unsigned int train_mode, unsigned int pulses_per_train, unsigned int pulse_period_cycles)
{
    unsigned int success;
    unsigned int pulse_gen_onoff;
    unsigned int trigger_event_onoff;
    unsigned int pulse_period_cycles_used;
    unsigned int pulses_per_train_used;
    unsigned int debug_pulse_param;

    if (pulses_per_train > 1024) //Because only 10 bits available
    {
        std::cerr << "ERROR: Invalid parameter " << pulses_per_train << ". Pulses per Timestep cannot larger than 1024!" << endl;
        return 0;
    }

    if ((pulse_period_cycles < 2) || pulse_period_cycles > 65536)
    {
        std::cerr << "ERROR: Invalid parameter " << pulse_period_cycles << ". Pulse period clock cycles must between 2-65536!" << endl;
        return 0;
    }

    if (train_mode > 1)
    {
        std::cerr << "ERROR: Invalid pulse train mode " << train_mode << "! (0 or 1 only)" << endl;
        return 0;
    }

    if (debug_pulse_select > 2)
    {
        std::cerr << "ERROR: Invalid paramter " << debug_pulse_select << "! (0 = Real GPIO pulses, 1 = Internal Pulse Generator, 2 = Trigger Events" << endl;
        return 0;
    }

    //Turn on or off debug generator
    pulse_gen_onoff = debug_pulse_select & 0x1;

    pulse_period_cycles_used = pulse_period_cycles & 0xFFFF; //Mask out 16 bits
    pulses_per_train_used = pulses_per_train & 0x3FF;        //Mask out 10 bits

    debug_pulse_param = (pulse_gen_onoff << PULSE_GEN_ONOFF_POS) + (train_mode << PULSE_TRAIN_MODE_POS) + (pulses_per_train_used << PULSES_PER_TRAIN_POS) + pulse_period_cycles_used;
    success = ADQ_WriteUserRegister(adq_cu, adq_num, 2, DEBUG_PULSE_GENERATOR_ADDR, 0, debug_pulse_param, NULL);

    //Turn on or off trigger events as debug pulses
    trigger_event_onoff = debug_pulse_select >> 1u;
    success = ADQ_WriteUserRegister(adq_cu, adq_num, 2, DEBUG_PULSE_GENERATOR_ADDR, ~(trigger_event_onoff << TRIGGER_EVENT_SELECT_POS), (trigger_event_onoff << TRIGGER_EVENT_SELECT_POS), NULL);

    return success;
}

//Deprecated in the new design where all GPIO are selected.
unsigned int Numbat_SelectGPIO(void *adq_cu, int adq_num, unsigned int gpio_pin_mask)
{
    unsigned int success;
    if (gpio_pin_mask > 0xFFF)
    {
        std::cerr << "ERROR: Invalid paramter " << gpio_pin_mask << "! GPIO mask out of range. Valid range: 0x000-0xFFF(0 = Real GPIO pulses, 1 << endl= Internal Pulse Generator, 2 = Trigger Events as pulses)\n"
                     << endl;
        return 0;
    }

    //Make sure to set the used GPIO as input. Here we lock the mask to only apply changes to the 12 first bits and use the input mask as direction instead.
    success = ADQ_SetDirectionGPIO(adq_cu, adq_num, gpio_pin_mask, ~0xFFF);

    return success;
}

void noscanf()
{
    //Dummy function
}
