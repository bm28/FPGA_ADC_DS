#include "Sps/Include/sps.h"
#include "Sps/Include/spec_shm.h"
#include <unistd.h>
#include <iostream>
#include <cstdlib>
#include <string>
#include <vector>
#include <random>
#include <map>
#include <cmath>
#include <iomanip>

#include <math.h> /* sin */

// using namespace std;

int main(int argc, char *argv[])
{
    char spec_version[] = "FpgaDS";
    char arrayname[] = "WaveForms";
    // char arrayname[] = "SCAN_D";
    int samples_per_waveform, counters, type, flag, c, r, x;
    void *myptr;
    void *spsptr;

    counters = 16;  // Time, A0-A3 and D0-D7
    samples_per_waveform = 256;     // max from card
    type = SPS_INT; // the data from the card are int
    flag = SPS_TAG_SCAN;

    myptr = malloc(samples_per_waveform * counters * sizeof(int));

    for (c = 0; c < counters; c++)
    {
        // std::cout << "row " << c << std::endl;
        for (r = 0; r < samples_per_waveform; r++)
        {
            *((int *)myptr + r * counters + c) = c * counters + r;
        }
    }

    SPS_CreateArray(spec_version, arrayname, samples_per_waveform, counters, type, flag);
    //char *metastr;
    u32_t length;
    int SCAN_N = 20;
    int actual, retval;
    void *thisptr;

//            std::string metastr  = "[{\"0\": \"Time\", \"1\": \"a0\", \"2\": \"a1\", \"3\": \"a2\", \"4\": \"a3\", \"5\": \"d0\", \"6\": \"d1\", \
// \"7\": \"d2\", \"8\": \"d3\", \"9\": \"d4\", \"10\": \"d5\", \"11\": \"d6\", \"12\": \"d7\", \"13\": \"d8\"}, \
// {\"allcounters\": \"sec;a0;a1;a2;a3;d0;d1;d2;d3;d4;d5;d6;d7;d8\", \"allmotorm\": \"\", \"allmotors\": \"\", \"allpositions\": \
// \"\", \"ctime\": 1, \"datafile\": \"/dev/null\", \"datapath\": \"/dev/null\", \"date\": \"Fri Jan 11 08:45:35 2019\", \
// \"hkl\": \"\", \"motors\": \"\", \"samples_per_waveform\": 4096, \"scanno\": 18, \"scanrange\": 0, \"scantype\": 144, \
// \"selectedcounters\": \"d0\", \"starts\": \"\", \"stops\": \"\", \"title\": \"timescan 1 0\", \"user\": \"witsch\"}, \
// {\"cntinfilter\": \"\", \"dotsize\": 5, \"linewid\": 2, \"mode\": 21507}]";

    const std::string metastrR = R"([{"0": "a0", "1": "a1", "2": "a2", "3": "a3", "4": "d0", "5": "d1", "6": "d2", "7": "d3", "8": )"
                                R"("d4", "9": "d5", "10": "d6", "11": "d7", "12": "d8", "13": "d9", "14": "d10", "15": "d11"}, )"
                                R"({"allcounters": "a0;a1;a2;a3;d0;d1;d2;d3;d4;d5;d6;d7;d8;d9;d10;d11", "allmotorm": "", )"
                                R"("allmotors": "", "allpositions": "", "motors": "", "samples_per_waveform": )"
                                + std::to_string(samples_per_waveform) + ", \"scanno\": " + std::to_string(SCAN_N) + ", \"scanrange\": 0, "
                                R"("scantype": 144, "selectedcounters": "", "starts": "", "stops": "", "title": "BALBALBA", )"
                                R"("user": "whoever"}, {"cntinfilter": "", "dotsize": 5, "linewid": 2}])";

    const char *thismeta = metastrR.c_str();
    char *meta;
    length = (u32_t)metastrR.length();
    std::cout << "SPS_PutMetaData length " << length << std::endl;

    if (SPS_PutMetaData(spec_version, arrayname, (char *)thismeta, length))
    {
        std::cerr << "SPS_PutMetaData failed!" << std::endl;
        throw;
    }

    if ((meta = SPS_GetMetaData(spec_version, arrayname, &length)) == NULL)
    {
        std::string const msg = "SPS_GetMetaData failed!";
        throw msg; // std::runtime_error("Error: " + msg + " " + std::string(retval);
    }
    std::cout << "meta " << meta << std::endl;

    std::cout << "straight line" << std::endl;
    if (SPS_CopyToShared(spec_version, arrayname, myptr, type, samples_per_waveform * counters))
    {
        std::string const msg = "SPS_CopyToShared failed!";
        throw msg; // std::runtime_error("Error: " + msg + " " + std::string(retval);
    }

    samples_per_waveform = 80;
    if (SPS_CreateArray(spec_version, arrayname, samples_per_waveform, counters, type, flag))
    {
        std::string const msg = "SPS_CopyToShared failed!";
        throw msg; // std::runtime_error("Error: " + msg + " " + std::string(retval);
    }
    if (SPS_PutMetaData(spec_version, arrayname, (char *)thismeta, length))
    {
        std::string const msg = "SPS_PutMetaData failed!";
        throw msg; // std::runtime_error("Error: " + msg + " " + std::string(retval);
    }
    sleep(5);

    // int maxx = samples_per_waveform * counters * 100;
    int maxx = 0;
    x = 0;

    std::cout << "straight descending line" << std::endl;
    for (r = 0; r < samples_per_waveform; r++)
    {
        // std::cout << "row " << c << std::endl;
        for (c = 0; c < counters; c++)
        {
            x = maxx + samples_per_waveform - c * 100 - r;
            thisptr = ((int *)myptr + r * counters + c);
            // std::cout << "p " << thisptr << " v " << x << " | " << std::flush;
            *(int *)thisptr = x;
        }
        if ((retval = SPS_CopyRowToShared(spec_version, arrayname, ((int *)myptr + r * counters), type, r, sizeof(int) * counters, &actual)))
        {
            std::string const msg = "SPS_CopyToShared failed!";
            throw msg; // std::runtime_error("Error: " + msg + " " + std::string(retval);
        }
        // std::cout << std::endl;
        usleep(100000);
    }
    sleep(5);

    std::cout << "sinus" << std::endl;
    const double PI = std::acos(-1);
    for (r = 0; r < samples_per_waveform; r++)
    {
        // std::cout << "col " << r << std::endl;
        /* these loops are organized in the spec scanning way, which means, we add a column to
         * a number of columns, each with 13 point up to 256 rows.
         */
        for (c = 0; c < counters; c++)
        {
            thisptr = ((int *)myptr + r * counters + c);

            x = sin((r + c * samples_per_waveform) * PI / 45) * 100000;
            // *((int *)myptr + r * counters + c) = x;
            *(int *)thisptr = x;
            // std::cout << " " << x;
        }
        // std::cout << std::endl;
        // try to write just one curve ---- use CopyDataRC
        // if (SPS_CopyToShared(spec_version, arrayname, myptr, type, samples_per_waveform * counters)) { // writes all data
        if ((retval = SPS_CopyRowToShared(spec_version, arrayname, ((int *)myptr + r * counters), type, r, sizeof(int) * counters, &actual)))
        {
            std::string const msg = "SPS_CopyToShared failed!";
            throw msg; // std::runtime_error("Error: " + msg + " " + std::string(retval);
        }
        usleep(100000);
    }
    sleep(5);

    // try SPS_IS_IMAGE
    char imgname[] = "a0";

    int height = 200, h;
    samples_per_waveform = 256;     // max from card
    type = SPS_INT; // the data from the card are int

    void *imgptr;
    imgptr = malloc(height * samples_per_waveform * sizeof(int));

    flag = SPS_TAG_IMAGE;

    std::cout << "IMAGE" << std::endl;
    char spec_name[] = "FpgaDS_SCAN";
    // spec_version = spec_version.c_str();

    SPS_CreateArray(spec_name, imgname, samples_per_waveform, height, type, flag);

    for (r = 0; r < samples_per_waveform; r++)
    {
        for (h = 0; h < height; h++)
        {
            *((int *)imgptr + r * height + h) = h * height + r * samples_per_waveform;
            // if (h == 0)
            // {
            //     std::cout << "h " << h << " r " << r << " " << ((int *)imgptr + r * height + h) << std::endl;
            // }
        }
        // std::cout << std::endl;

        // what now ????
        /*
            Copy a row of data to the shared memory array from a buffer.
            Input:
                name : The name of the specversion
                array : The name of the array in this SPEC version
                buf : A pointer to the buffer.
                my_type : The data type how you would like to have the buffer. This
                    data type does not have to be the same as the data type
                    of the shared memory array. In this case it will be
                    transformed in the data type you asked for (fast)
                row : integer. Which row do you want.
                col : integer. How many columns you want to copy. If this is 0 then
                    all the columns are copied.
                act_cols : pointer to int. If not NULL then this is filled with the
                        number of columns actually copied.
            Returns: 0 success
                    1 error
        */

        if ((retval = SPS_CopyRowToShared(spec_name, imgname, ((int *)imgptr + r * height), type, r, sizeof(int) * height, &actual)))
        {
            std::string const msg = "SPS_CopyRowToShared failed!";
            // throw msg; // std::runtime_error("Error: " + msg + " " + std::string(retval);
            throw std::runtime_error("Error: " + msg + " " + std::to_string(retval));
        }
        usleep(100000);
    }

    // if (SPS_CopyToShared(spec_version, imgname, imgptr, type, height * samples_per_waveform))
    // {
    //     std::string const msg = "SPS_CopyToShared failed!";
    //     throw std::runtime_error("Error: " + msg + " " + string(retval));
    // }
    // std::cout << "Copied all" << std::endl;
    sleep(600);
}
